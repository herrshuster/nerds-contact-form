<?php

class NerdsCF {

  public $id;

  public $html;

  public $css;

  public $embedCode;

  public $alias;

  public $confirmation;

  public $confirmation_is_link;

  private $iframeWidth;

  private $iframeHeight;

  private $iframeStyle;

  public function __construct( $id ) {

    global $wpdb;

    $this->id = $id;

    $db = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}nerds_contact_forms WHERE id = '{$this->id}'" );

    $this->html = $db[0]->html;

    $this->css = $db[0]->css;

    $this->iframeWidth = $db[0]->iframe_width;

    $this->iframeHeight = $db[0]->iframe_height;

    $this->alias = $db[0]->alias;

    $this->confirmation = $db[0]->confirmation;

    $this->confirmation_is_link = substr( $this->confirmation, 0, 3 ) == "htt" ? true : false;

    $this->iframeStyle = str_replace( '\n', '', $db[0]->iframe_style );

    $this->embedCode = "<iframe style='{$this->iframeStyle}' width='{$this->iframeWidth}' height='{$this->iframeHeight}' src='{$this->buildURL()}'></iframe>";

  }

  private function buildURL() {

    return get_bloginfo( 'url' ) . "/contact_form/" . $this->id . "/";

  }

  public function getHTML() {

    ob_start();

    $NerdsCF = $this;

    require_once( NERDS_CF_DIR . "assets/php/default_contact_form.php" );

    return ob_get_clean();

  }

  public static function admin_menu_application_list() {

    add_menu_page(
      'Contact Form Variants', // Page Title
      'Nerds Contact Forms', // Menu Title
      'manage_options', // Capability
      'contact_forms', // Menu Slug
      'NerdsCF::admin_page_application_list' // Function
    );

  }

  public static function admin_page_application_list() {

    require( NERDS_CF_DIR . "assets/php/admin_page_application_list.php" );

  }

  public static function defaultEmbedCode( $id ) {

    // TODO: match height

    echo "<iframe src='%URL%' style='border: 2px solid black;background-color:white;border-radius:4px;' width='320' height='335'></iframe>";

  }

  public static function ajax_delete() {

    global $wpdb;

    $id = $_POST['id'];

    $wpdb->delete(
      $wpdb->prefix . "nerds_contact_forms",
      array(
        'id' => $id
      )
    );

    die();

  }

  public static function ajax_save() {

    global $wpdb;

    $id = $_POST['id'];

    $html = $_POST['html'];

    $css = $_POST['css'];

    $iframe_height = $_POST['iframe_height'];

    $iframe_width = $_POST['iframe_width'];

    $iframe_style = $_POST['iframe_style'];

    $alias = $_POST['alias'];

    $confirmation = $_POST['confirmation'];

    // echo print_r($_POST,true);
    //
    // die();
    //
    // return;

    if( $id == 'new' ) {

      $wpdb->insert(
        $wpdb->prefix . "nerds_contact_forms",
        array(
          'html' => $html,
          'css' => $css,
          'iframe_height' => $iframe_height,
          'iframe_width' => $iframe_width,
          'iframe_style' => $iframe_style,
          'alias' => $alias,
          'confirmation' => $confirmation
        )
      );

      $id = $wpdb->insert_id;


    } else {

      $wpdb->update(
        $wpdb->prefix . "nerds_contact_forms",
        array(
          'html' => $html,
          'css' => $css,
          'iframe_height' => $iframe_height,
          'iframe_width' => $iframe_width,
          'iframe_style' => $iframe_style,
          'alias' => $alias,
          'confirmation' => $confirmation
        ),
        array(
          'id' => $id
        )
      );

    }

    $response = array(
      'id' => $id
    );

    die( json_encode( $response ) );
  }


}
