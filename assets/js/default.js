function sendEmail(event) {

  event.preventDefault();

  var name = '';
  var email = '';
  var phone = '';
  var notes = '';

  if( document.getElementsByName('name').length > 0 ) name = document.getElementsByName('name')[0].value;
  if( document.getElementsByName('email').length > 0 ) email = document.getElementsByName('email')[0].value;
  if( document.getElementsByName('phone').length > 0 ) phone = document.getElementsByName('phone')[0].value;
  if( document.getElementsByName('notes').length > 0 ) notes = document.getElementsByName('notes')[0].value;


  var ajaxurl = 'https://callnerds.com/wp-admin/admin-ajax.php';

  var data = {
    action: 'nerds_contact_submit',
    name: name,
    email: email,
    phone: phone,
    notes: notes,
    variant: formID
  };


  jQuery.post(ajaxurl, data, function(response,status){
    // dataLayer.push({
    //   'event': 'VitualPageview',
    //   'virtualPageURL': '/success',
    //   'virtualPageTitle': 'Contact Us Page - Send Message - Form ' + formID,
    //   'eventCallback': function () {
    //
    //   }
    // });
    setTimeout(function(){
      successModal();
    },300);
  });

}

if( ! successModal ) {

function successModal() {

  var form = document.getElementById('contact_form');

  form.outerHTML = "<span class='thank-you'>Thanks for contacting us! We'll be in touch shortly</span>";

}

}

function failModal() {

  var form = document.getElementById('contact_form');

  form.outerHTML = "Oops! Something went wrong. Refresh the page or wait a few seconds and try again.";

  setTimeout(function() {
    location.reload(true);
  }, 4000);

}
