<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Contact Nerds On Call</title>
  <link rel="stylesheet" href="<?php echo NERDS_CF_URL; ?>assets/css/default.css" media="screen" title="Default stylesheet" charset="utf-8">
  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

  <style media="screen">
    <?php if( $NerdsCF ) echo stripslashes( $NerdsCF->css ); ?>
  </style>
</head>
<body>

  <?php if( ! $NerdsCF->confirmation_is_link ): ?>
  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PFB8L4"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-PFB8L4');</script>
  <!-- End Google Tag Manager -->
  <?php else: ?>
    <!-- Google Tag Manager -->
    <!-- <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PFB8L4"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MJ7BSK');</script> -->
    <!-- End Google Tag Manager -->

  <?php endif; ?>

  <?php if( $NerdsCF ) echo stripslashes( $NerdsCF->html ); ?>

</body>

<script type="application/javascript">

  <?php

  if( $NerdsCF ){

    echo "var formID = '" . $NerdsCF->id . "';";

  } else {

    echo "var formID = 0;";

  }

  ?>

  function successModal() {


    <?php if( $NerdsCF->confirmation_is_link ): ?>

      window.top.location.href = "<?php echo $NerdsCF->confirmation; ?>";

    <?php else: ?>

      var form = document.getElementById('contact_form');

      form.outerHTML = "<span class='thank-you'><?php echo $NerdsCF->confirmation; ?></span>";

    <?php endif; ?>

  }

  <?php echo file_get_contents( NERDS_CF_DIR . "/assets/js/default.js" ); ?>

</script>

</html>
