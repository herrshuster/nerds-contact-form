<?php

/*
Plugin Name: Nerds Contact Form
Plugin URI: https://bitbucket.org/herrshuster/nerds-contact-form
Description: Create contact form for Nerds On Call
Version: 0.1
Author: Tyler Shuster
Author URI: http://tyler.shuster.house
*/

/**
 * Released under the GPL0 license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

 define( 'NERDS_CF_DIR', plugin_dir_path( __FILE__ ) );
 define( 'NERDS_CF_URL', plugin_dir_url( __FILE__ ) );

require_once( 'database.php' );
require_once( 'class-nerds_cf.php' );

register_activation_hook( __FILE__, 'nerds_cf_create_table' );


add_action( 'wp_ajax_nerds_cf_save', 'NerdsCF::ajax_save' );

add_action( 'wp_ajax_nerds_cf_delete', 'NerdsCF::ajax_delete' );

add_action( 'admin_menu', 'NerdsCF::admin_menu_application_list' );


// http://wordpress.stackexchange.com/questions/9870/how-do-you-create-a-virtual-page-in-wordpress/9959#9959
function nerds_cf_rewrite() {

  add_rewrite_rule( "^contact_form/([0-9]+)/?", 'index.php?nerds_cf=$matches[1]', 'top' );

}

add_action( 'init', 'nerds_cf_rewrite' );

add_filter( 'query_vars', 'nerds_cf_query_vars' );
function nerds_cf_query_vars( $query_vars ) {

  $query_vars[] = 'nerds_cf';

  return $query_vars;

}

add_action( 'parse_request', 'nerds_cf_parse_request' );

function nerds_cf_parse_request( &$wp ) {

  if( array_key_exists( 'nerds_cf', $wp->query_vars ) ) {

    $form_id = $wp->query_vars['nerds_cf'];

    include 'api.php';

    exit();

  }

  return;

}
