<?php

global $wpdb;

require_once( NERDS_CF_DIR . "class-nerds_cf.php" );

?>

<style media="screen">
  <?php echo file_get_contents( NERDS_CF_DIR . "/assets/css/admin.css" ); ?>
</style>

<span class="nerds_cf--inputs">Available input names: name, email, phone, notes</span>

<ol class="nerds_cf--list">

  <?php

  $ContactForms = $wpdb->get_results( "SELECT id FROM {$wpdb->prefix}nerds_contact_forms" );

  foreach( $ContactForms as $ContactForm ){

    $ContactForm = new NerdsCF( $ContactForm->id );

    ?>

    <li><a href="?page=contact_forms&amp;id=<?php echo $ContactForm->id; ?>"><?php echo $ContactForm->alias ? $ContactForm->alias : "Variant " . $ContactForm->id; ?></a> (ID: <?php echo $ContactForm->id; ?>)</li>

    <?php

  }

   ?>

</ol>


<?php

if( isset( $_GET['id'] ) ){

  $ContactForm = new NerdsCF( intval( $_GET['id'] ) );

  if( $ContactForm->id ):

  ?>

  <form id="<?php echo $_GET['id']; ?>" class="nerds_cf--form">

    <h2>Form <?php echo $ContactForm->id; ?></h2>

    <input type="text" name="alias" placeholder='Enter a name for this contact form' class="nerds_cf--alias" value="<?php echo $ContactForm->alias; ?>">

    <h2>Form Content</h2>
    <textarea class="nerds_cf--content"><?php echo stripslashes( htmlspecialchars($ContactForm->html) ); ?></textarea>

    <h2>Form Style</h2>
    <textarea class="nerds_cf--style"><?php echo stripslashes( htmlspecialchars($ContactForm->css) ); ?></textarea>

    <h2>iFrame Style (CSS)</h2>
    <textarea class="nerds_cf--iframe_style"><?php echo stripslashes( htmlspecialchars($ContactForm->iframeStyle) ); ?></textarea>

    <h2>Confirmation</h2>
    <small>Enter a URL to redirect, or text</small>
    <input type="text" class="nerds_cf--confirmation" name="confirmation" value="<?php echo stripslashes( htmlspecialchars( $ContactForm->confirmation ) ); ?>">

    <label for="iframe_height">iFrame Height</label>
    <input type="number" name="iframe_height" class="nerds_cf--iframe_height" value="<?php echo $ContactForm->iframeHeight; ?>">
    <label for="iframe_width">iFrame Width</label>
    <input type="number" name="iframe_width" class="nerds_cf--iframe_width" value="<?php echo $ContactForm->iframeWidth; ?>">


    <h2>iFrame Code</h2>
    <pre class="nerds_cf--iframe"><?php echo htmlspecialchars($ContactForm->embedCode); ?></pre>

    <button class="nerds_cf--duplicate">Duplicate</button>

    <button class="nerds_cf--delete">Delete</button>

  </form>


  <?php

  endif;

}

 ?>


  <button id="new_cf">New Variant</button>


<script type="application/javascript">

  jQuery(document).ready(function($) {

    function updateIframeEmbed() {

      var style = $('.nerds_cf--iframe_style').val();
      var width = $('.nerds_cf--iframe_width').val();
      var height = $('.nerds_cf--iframe_height').val();
      var id = $('.nerds_cf--form')[0].id;
      var code = '';

      $('.nerds_cf--iframe').text( "<iframe style='" + style + "' width='" + width + "' height='" + height + "' src='<?php echo get_bloginfo("url") . "/contact_form/"; ?>" + id + "'></iframe>" );

    }

    var activeCF = false;

    var blankCF = " <form class='nerds_cf--form' id='new'>\
                      <input type='text' name='alias' class='nerds_cf--alias' placeholder='Enter a name for this contact form' class='nerds_cf--alias' value=''>\
                      <h2>Form Content</h2>\
                      <textarea class='nerds_cf--content'></textarea>\
                      <h2>Form Style</h2>\
                      <textarea class='nerds_cf--style'></textarea>\
                      <h2>Confirmation</h2>\
                      <small>Enter a URL to redirect, or text</small>\
                      <input type='text' class='nerds_cf--confirmation' name='confirmation' value=''>\
                      <h2>iFrame Style (CSS)</h2>\
                      <textarea class='nerds_cf--iframe_style'></textarea>\
                      <label for='iframe_height'>iFrame Height</label>\
                      <input type='number' name='iframe_height' class='nerds_cf--iframe_height' value=''>\
                      <label for='iframe_width'>iFrame Width</label>\
                      <input type='number' name='iframe_width' class='nerds_cf--iframe_width' value=''>\
                      <button class='nerds_cf--duplicate'>Duplicate</button>\
                      <button class='nerds_cf--delete'>Delete</button>\
                    </form>";

    if( $('.nerds_cf--form').length > 0 ) {

      activeCF = $( '.nerds_cf--form' )[0]['id'];

    }

    $('#new_cf').click( function() {

      if( activeCF ) {

        $( '#' + activeCF ).replaceWith(blankCF);

      } else {

        $('#new_cf').before( blankCF );

      }
    });

    $("body").on('submit', '.nerds_cf--form', function(event){
      event.preventDefault();
    });

    function saveForm() {

      var id = $('.nerds_cf--form')[0].id;

      if( ! id ) {

        id = 'new';

      }

      var data = {
        'action': 'nerds_cf_save',
        'id': id,
        'alias': $('.nerds_cf--alias').val(),
        'html': $('.nerds_cf--content').val(),
        'css': $('.nerds_cf--style').val(),
        'iframe_style': $(".nerds_cf--iframe_style").val(),
        'iframe_height': $(".nerds_cf--iframe_height").val(),
        'iframe_width': $(".nerds_cf--iframe_width").val(),
        'confirmation': $(".nerds_cf--confirmation").val()
      };

      console.log(data);

      $.post( ajaxurl, data, function(response, status){

        console.log(response,status);

        response = JSON.parse( response );

        $('.nerds_cf--form').attr( 'id', response.id );

      });
    }


    $('body').on( 'click', '.nerds_cf--delete', function(event) {

      var id = $('.nerds_cf--form')[0].id;

      if( ! id ) {

        id = 'new';

      }

      if( id == 'new' ) {

        $('.nerds_cf--form').remove();

      } else {

        var data = {
          'action': 'nerds_cf_delete',
          'id': id
        };

        $.post( ajaxurl, data, function( response, status ) {

          if( status == 'success' ) $( '.nerds_cf--form' ).remove();

          window.location.reload(false);

        });
      }
    });

    $('body').on( 'click', '.nerds_cf--duplicate', function( event ) {

      var data = {
        'action': 'nerds_cf_save',
        'id': 'new',
        'alias': $('.nerds_cf--alias').val(),
        'html': $('.nerds_cf--content').val(),
        'css': $('.nerds_cf--style').val(),
        'iframe_style': $(".nerds_cf--iframe_style").val(),
        'iframe_height': $(".nerds_cf--iframe_height").val(),
        'iframe_width': $(".nerds_cf--iframe_width").val(),
        'confirmation': $(".nerds_cf--confirmation").val()
      };

      $.post( ajaxurl, data, function(response, status){

        response = JSON.parse( response );

        $('.nerds_cf--form').attr( 'id', response.id );

        $('.nerds_cf--list').append("<li><a href='?page=contact_forms&amp;id=" + response.id + "'>Variant " + response.id + "</a></li>");

      });

    });


    $('body').on( 'change', '.nerds_cf--alias, .nerds_cf--confirmation, .nerds_cf--content, .nerds_cf--style, .nerds_cf--iframe_style, .nerds_cf--iframe_height, .nerds_cf--iframe_width', function(event) {

      saveForm();

      updateIframeEmbed();

    });



  });
</script>
