<?php

if( ! function_exists( 'nerds_cf_create_table' ) ) {

  function nerds_cf_create_table() {

    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();

    $prefix = $wpdb->prefix . "nerds_contact_forms";

    $sql = <<<"SQL"

    CREATE TABLE $prefix (
    id INT(6) UNSIGNED AUTO_INCREMENT,
    PRIMARY KEY (id),
    alias VARCHAR(50),
    confirmation VARCHAR(120),
    html TEXT,
    css TEXT,
    iframe_style varchar(200),
    iframe_width int(4),
    iframe_height int(4)
    ) $charset_collate;
SQL;

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    dbDelta( $sql );

  }

}
